﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PrimerWpf
{
    public enum TipoDeSwitch {
        [Description("SC1")]
        Seccionador1 = 0,
        [Description("SC2")]
        Seccionador2,
        [Description("DYT")]
        Disyuntor
    }
    public class Controles : INotifyPropertyChanged
    {
        private SolidColorBrush sw1 = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw2 = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush dyt = new SolidColorBrush(Colors.LightGray);
        private String estacion = "Estacion Principal";
        private String voltaje = "";
        private String corriente = "";
        private bool isEnabled = true;
        public event PropertyChangedEventHandler PropertyChanged;
        public SolidColorBrush Sw1
        {
            get { return sw1; }
            set
            {
                if (sw1 != value)
                {
                    sw1 = value;
                    NotifyPropertyChanged("Sw1");
                }
            }
        }
        public SolidColorBrush Sw2
        {
            get { return sw2; }
            set
            {
                if (sw2 != value)
                {
                    sw2 = value;
                    NotifyPropertyChanged("Sw2");
                }
            }
        }
        public SolidColorBrush Dyt
        {
            get { return dyt; }
            set
            {
                if (dyt != value)
                {
                    dyt = value;
                    NotifyPropertyChanged("Dyt");
                }
            }
        }
        public String Estacion
        {
            get { return estacion; }
            set
            {
                if (estacion != value)
                {
                    estacion = value;
                    NotifyPropertyChanged("Estacion");
                }
            }
        }
        public String Voltaje {
            get { return voltaje; }
            set {
                if (voltaje != value)
                {
                    voltaje = value;
                    NotifyPropertyChanged("Voltaje");
                }
            }
        }
        public String Corriente
        {
            get { return corriente; }
            set
            {
                if (corriente != value)
                {
                    corriente = value;
                    NotifyPropertyChanged("Corriente");
                }
            }
        }
        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                if(isEnabled != value)
                {
                    isEnabled = value;
                }
            }
        }
        public void NotifyPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
