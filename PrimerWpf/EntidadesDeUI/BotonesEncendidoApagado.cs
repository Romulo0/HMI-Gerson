﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimerWpf.EntidadesDeUI
{
    class BotonesEncendidoApagado
    {
        #region Variables
        private Boolean encendido = false;
        private Boolean apagado = false;
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region Propiedades
        public Boolean HabilitarEncendido
        {
            get { return encendido; }
            set
            {
                if (encendido != value)
                {
                    encendido = value;
                    NotifyPropertyChanged("HabilitarEncendido");
                }
            }
        }
        public Boolean HabilitarApagado
        {
            get { return apagado; }
            set
            {
                if (apagado != value)
                {
                    apagado = value;
                    NotifyPropertyChanged("HabilitarApagado");
                }
            }
        }
        #endregion
        #region Metodos
        public void NotifyPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        #endregion
    }
}
