﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimerWpf.EntidadesDeUI
{
    class ControlDeSesion : INotifyPropertyChanged
    {
        private string usuario;
        private string link;
        public event PropertyChangedEventHandler PropertyChanged;
        public string Usuario {
            get { return usuario; }
            set
            {
                if(usuario != value)
                {
                  usuario  = value;
                  NotifyPropertyChanged("Usuario");
                }
            }
        }
        public string Link
        {
            get { return link; }
            set
            {
                if (link != value)
                {
                    link = value;
                    NotifyPropertyChanged("Link");
                }
            }
        }

        public void NotifyPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
