﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PrimerWpf
{
    public class IndicadoresDeEstacionPrincipal : INotifyPropertyChanged
    {
        #region Variables
        private SolidColorBrush sw1Principal = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw2Principal = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw1Primera = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw2Primera = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw1Segunda = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw2Segunda = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw1Tercera = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw2Tercera = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw1Cuarta = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw2Cuarta = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw1Quinta = new SolidColorBrush(Colors.LightGray);
        private SolidColorBrush sw2Quinta = new SolidColorBrush(Colors.LightGray);
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
        #region Propiedades
        public SolidColorBrush Sw1Principal {
                get { return sw1Principal; }
                set {
                    if (sw1Principal != value){
                        sw1Principal = value;
                        NotifyPropertyChanged("Sw1Principal");
                    }
                }
            }
        public SolidColorBrush Sw2Principal {
                get { return sw2Principal; }
                set {
                    if (sw2Principal != value) {
                        sw2Principal = value;
                        NotifyPropertyChanged("Sw2Principal");
                    }
                }
            }
        public SolidColorBrush Sw1Primera
        {
            get { return sw1Primera; }
            set
            {
                if (sw1Primera != value)
                {
                    sw1Primera = value;
                    NotifyPropertyChanged("Sw1Primera");
                }
            }
        }
        public SolidColorBrush Sw2Primera
        {
            get { return sw2Primera; }
            set
            {
                if (sw2Primera != value)
                {
                    sw2Primera = value;
                    NotifyPropertyChanged("Sw2Primera");
                }
            }
        }
        public SolidColorBrush Sw1Segunda
        {
            get { return sw1Segunda; }
            set
            {
                if (sw1Segunda != value)
                {
                    sw1Segunda = value;
                    NotifyPropertyChanged("Sw1Segunda");
                }
            }
        }
        public SolidColorBrush Sw2Segunda
        {
            get { return sw2Segunda; }
            set
            {
                if (sw2Segunda != value)
                {
                    sw2Segunda = value;
                    NotifyPropertyChanged("Sw2Segunda");
                }
            }
        }
        public SolidColorBrush Sw1Tercera
        {
            get { return sw1Tercera; }
            set
            {
                if (sw1Tercera != value)
                {
                    sw1Tercera = value;
                    NotifyPropertyChanged("Sw1Tercera");
                }
            }
        }
        public SolidColorBrush Sw2Tercera
        {
            get { return sw2Tercera; }
            set
            {
                if (sw2Tercera != value)
                {
                    sw2Tercera = value;
                    NotifyPropertyChanged("Sw2Tercera");
                }
            }
        }
        public SolidColorBrush Sw1Cuarta
        {
            get { return sw1Cuarta; }
            set
            {
                if (sw1Cuarta != value)
                {
                    sw1Cuarta = value;
                    NotifyPropertyChanged("Sw1Cuarta");
                }
            }
        }
        public SolidColorBrush Sw2Cuarta
        {
            get { return sw2Cuarta; }
            set
            {
                if (sw2Cuarta != value)
                {
                    sw2Cuarta = value;
                    NotifyPropertyChanged("Sw2Cuarta");
                }
            }
        }
        public SolidColorBrush Sw1Quinta
        {
            get { return sw1Quinta; }
            set
            {
                if (sw1Quinta != value)
                {
                    sw1Quinta = value;
                    NotifyPropertyChanged("Sw1Quinta");
                }
            }
        }
        public SolidColorBrush Sw2Quinta
        {
            get { return sw2Quinta; }
            set
            {
                if (sw2Quinta != value)
                {
                    sw2Quinta = value;
                    NotifyPropertyChanged("Sw2Quinta");
                }
            }
        }
        #endregion
        #region Metodos
        public void NotifyPropertyChanged(string propName) {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }
        #endregion
    }
}
