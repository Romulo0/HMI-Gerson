﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrimerWpf.Clases_Generales;

namespace PrimerWpf
{
    public partial class ControlSwitch : Window
    {
        #region Variables
        private bool _Estado = false;
        private TipoDeSwitch _Switch;
        private enumEstaciones estacion;
        private ComunicacionSerial comunicacionMicro;
        private Controles controles = new Controles();
        private Modelo modelo = new Modelo();
        #endregion
        #region Constructores
        public ControlSwitch(ref ComunicacionSerial comunicacionSerial, enumEstaciones _estacion)
        {
            InitializeComponent();
            DataContext = controles;
            ComunicacionMicro = comunicacionSerial;
            estacion =  _estacion;
        }
        #endregion
        #region Propiedades
        public ComunicacionSerial ComunicacionMicro {
            get { return comunicacionMicro; }
            set
            {
                if(comunicacionMicro != value)
                {
                    comunicacionMicro = value;
                }
            }

        }
        public bool Estado {
            get {
                return _Estado;
            }
            set {
                if (_Estado != value) {
                    _Estado = value;
                }
            }
        }
        public TipoDeSwitch Switch {
            get {
                return _Switch;
            }
            set{
                if(_Switch != value){
                    _Switch = value;
                }
            }
        }
        #endregion
        #region Eventos Click
        private void Button_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            Close();
        }

        private void Encender_Click(object sender, RoutedEventArgs e)
        {
            try { 
                Estado = true;
                EjecutaAccionSegunTipoDeSwitch(Estado);
                MessageBox.Show("Encendido");
            }catch(Exception ex){
                MessageBox.Show(ex.Message);
            }
}

        private void Apagar_Click(object sender, RoutedEventArgs e)
        {
            try { 
                Estado = false;
                EjecutaAccionSegunTipoDeSwitch(Estado);
                MessageBox.Show("Apagado");
            }catch(Exception ex){
                MessageBox.Show(ex.Message);
            }
        }
        private void EjecutaAccionSegunTipoDeSwitch(bool Accion) {
            switch (Switch) {
                case (TipoDeSwitch.Disyuntor):
                    if (!Accion) {
                        ComunicacionMicro.EnviarMensaje("B");
                        InsertaAccion((int)enumAcciones.DisyuntorOff);
                    }else {
                        ComunicacionMicro.EnviarMensaje("A");
                        InsertaAccion((int)enumAcciones.DisyuntorOn);
                    }
                    break;
                case (TipoDeSwitch.Seccionador1):
                    if (!Accion) {
                        ComunicacionMicro.EnviarMensaje("D");
                        InsertaAccion((int)enumAcciones.Seccionador1Off);
                    }
                    else{
                        ComunicacionMicro.EnviarMensaje("C");
                        InsertaAccion((int)enumAcciones.Seccionador1On);
                    }
                    break;
                case (TipoDeSwitch.Seccionador2):
                    if (!Accion) {
                        ComunicacionMicro.EnviarMensaje("F");
                        InsertaAccion((int)enumAcciones.Seccionador2Off);
                    }
                    else{
                        ComunicacionMicro.EnviarMensaje("E");
                        InsertaAccion((int)enumAcciones.Seccionador2On);
                    }
                    break;
                default:
                    break;
            }
        }
        private async void InsertaAccion(int TipoDeAccion)
        {
            controles.IsEnabled = false;
            await modelo.InsertarAccionDeUsuario(TipoDeAccion, (int)estacion);
            controles.IsEnabled = true;
        }
        #endregion
    }
}
