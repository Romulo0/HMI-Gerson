﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrimerWpf.EntidadesDeUI;
using PrimerWpf.Clases_Generales;

namespace PrimerWpf
{
    public partial class SignUpForm : Window
    {
        private bool signUpIsValid = false;
        private bool toSignIn = false;
        private Modelo modelo = new Modelo();
        byte Intentos = 0;
        ControlDeSesion controlDeSesion = new ControlDeSesion();
        public SignUpForm()
        {
            DataContext = controlDeSesion;
            InitializeComponent();
        }
        public bool SignUpIsValid
        {
            get { return signUpIsValid; }
            set
            {
                if (signUpIsValid != value)
                {
                    signUpIsValid = value;
                }
            }
        }
        public bool ToSignIn
        {
            get { return toSignIn; }
            set
            {
                if (toSignIn != value)
                {
                    toSignIn = value;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("No inicio sesion, la aplicacion se cerrara");
            ToSignIn = false;
            SignUpIsValid = false;
            Close();
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {

                if (!await modelo.BuscarUsuario(controlDeSesion.Usuario, contraseña.Password))
                {
                    Intentos++;
                    if (Intentos > 4)
                    {
                        MessageBox.Show("Varios intentos incorrectos, la aplicacion se cerrara");
                        ToSignIn = false;
                        SignUpIsValid = false;
                        App.Current.Shutdown();
                    }
                    else
                        MessageBox.Show("Usuario o Contraseña incorrectos");
                }
                else
                {
                    ToSignIn = false;
                    SignUpIsValid = true;
                    Close();
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            ToSignIn = true;
            SignUpIsValid = false;
            Close();
        }
    }
}

