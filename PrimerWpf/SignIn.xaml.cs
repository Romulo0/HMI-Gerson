﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PrimerWpf.EntidadesDeUI;
using PrimerWpf.Clases_Generales;

namespace PrimerWpf
{
    public partial class SignIn : Window
    {
        private ControlDeSesion controlDeSesion = new ControlDeSesion();
        private Modelo modelo = new Modelo();
        private bool toSignUp = false;
        public SignIn()
        {
            DataContext = controlDeSesion;
            InitializeComponent();
        }
        public bool ToSignUp
        {
            get { return toSignUp; }
            set
            {
                if (toSignUp != value)
                {
                    toSignUp = value;
                }
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            toSignUp = false;
            Close();
        }
   
        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                if (controlDeSesion.Usuario == "" || contraseña.Password == "" || confirmacion.Password == "")
                {
                    MessageBox.Show("No se permiten campos vacios");
                    return;
                }
                if (contraseña.Password != confirmacion.Password)
                {
                    MessageBox.Show("Las contraseñas no coinciden");
                    return;
                }
                await modelo.InsertarUsuario(controlDeSesion.Usuario, contraseña.Password);
                MessageBox.Show("Se ha registrado con exito");
                InvocarFormularioSignUp();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            InvocarFormularioSignUp();
        }
        private void InvocarFormularioSignUp()
        {
            toSignUp = true;
            Close();
        }
    }
}
