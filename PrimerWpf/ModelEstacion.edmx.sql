
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/20/2018 15:36:33
-- Generated from EDMX file: C:\Users\romul\OneDrive\Documents\Visual Studio 2017\Projects\PrimerWpf\PrimerWpf\ModelEstacion.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HMICentralCORPOELEC];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Accion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserAccionsSet] DROP CONSTRAINT [FK_Accion];
GO
IF OBJECT_ID(N'[dbo].[FK_SignUp]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserAccionsSet] DROP CONSTRAINT [FK_SignUp];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuariosSet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SignUpSet] DROP CONSTRAINT [FK_UsuariosSet];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[SignUpSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SignUpSet];
GO
IF OBJECT_ID(N'[dbo].[UserAccionsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserAccionsSet];
GO
IF OBJECT_ID(N'[dbo].[AccionesSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccionesSet];
GO
IF OBJECT_ID(N'[dbo].[UsuariosSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsuariosSet];
GO
IF OBJECT_ID(N'[dbo].[EstacionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EstacionSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'SignUpSet'
CREATE TABLE [dbo].[SignUpSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaDeInicio] datetime  NOT NULL,
    [FechaDeCierre] datetime  NOT NULL,
    [IdUsuario] int  NOT NULL
);
GO

-- Creating table 'UserAccionsSet'
CREATE TABLE [dbo].[UserAccionsSet] (
    [IdSignUp] int  NOT NULL,
    [Accion] int  NOT NULL,
    [FechaDeEjecucion] datetime  NOT NULL,
    [Id] int  IDENTITY(1,1) NOT NULL,
    [IdEstacion] int  NOT NULL
);
GO

-- Creating table 'AccionesSet'
CREATE TABLE [dbo].[AccionesSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Dispositivo] nvarchar(max)  NOT NULL,
    [Estado] bit  NOT NULL
);
GO

-- Creating table 'UsuariosSet'
CREATE TABLE [dbo].[UsuariosSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Username] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'EstacionSet'
CREATE TABLE [dbo].[EstacionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'SignUpSet'
ALTER TABLE [dbo].[SignUpSet]
ADD CONSTRAINT [PK_SignUpSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UserAccionsSet'
ALTER TABLE [dbo].[UserAccionsSet]
ADD CONSTRAINT [PK_UserAccionsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AccionesSet'
ALTER TABLE [dbo].[AccionesSet]
ADD CONSTRAINT [PK_AccionesSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UsuariosSet'
ALTER TABLE [dbo].[UsuariosSet]
ADD CONSTRAINT [PK_UsuariosSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EstacionSet'
ALTER TABLE [dbo].[EstacionSet]
ADD CONSTRAINT [PK_EstacionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Accion] in table 'UserAccionsSet'
ALTER TABLE [dbo].[UserAccionsSet]
ADD CONSTRAINT [FK_Accion]
    FOREIGN KEY ([Accion])
    REFERENCES [dbo].[AccionesSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Accion'
CREATE INDEX [IX_FK_Accion]
ON [dbo].[UserAccionsSet]
    ([Accion]);
GO

-- Creating foreign key on [IdSignUp] in table 'UserAccionsSet'
ALTER TABLE [dbo].[UserAccionsSet]
ADD CONSTRAINT [FK_SignUp]
    FOREIGN KEY ([IdSignUp])
    REFERENCES [dbo].[SignUpSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SignUp'
CREATE INDEX [IX_FK_SignUp]
ON [dbo].[UserAccionsSet]
    ([IdSignUp]);
GO

-- Creating foreign key on [IdUsuario] in table 'SignUpSet'
ALTER TABLE [dbo].[SignUpSet]
ADD CONSTRAINT [FK_UsuariosSet]
    FOREIGN KEY ([IdUsuario])
    REFERENCES [dbo].[UsuariosSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
ALTER TABLE [dbo].[UserAccionsSet]
ADD CONSTRAINT [FK_Estacion]
    FOREIGN KEY ([IdEstacion])
    REFERENCES [dbo].[EstacionSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO
-- Creating non-clustered index for FOREIGN KEY 'FK_UsuariosSet'
CREATE INDEX [IX_FK_UsuariosSet]
ON [dbo].[SignUpSet]
    ([IdUsuario]);
GO
INSERT [dbo].[AccionesSet](Estado, Dispositivo)VALUES(0,"DisyuntorOFF");
INSERT [dbo].[AccionesSet](Estado, Dispositivo)VALUES(1,"DisyuntorON");
INSERT [dbo].[AccionesSet](Estado, Dispositivo)VALUES(0,"Seccionador1OFF");
INSERT [dbo].[AccionesSet](Estado, Dispositivo)VALUES(1,"Seccionador1ON");
INSERT [dbo].[AccionesSet](Estado, Dispositivo)VALUES(0,"Seccionador2OFF");
INSERT [dbo].[AccionesSet](Estado, Dispositivo)VALUES(1,"Seccionador2ON");
GO
INSERT [dbo].[EstacionSet](Nombre)VALUES("Estacion Principal");
INSERT [dbo].[EstacionSet](Nombre)VALUES("Estacion 1");
INSERT [dbo].[EstacionSet](Nombre)VALUES("Estacion 2");
INSERT [dbo].[EstacionSet](Nombre)VALUES("Estacion 3");
INSERT [dbo].[EstacionSet](Nombre)VALUES("Estacion 4");
INSERT [dbo].[EstacionSet](Nombre)VALUES("Estacion 5");

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------