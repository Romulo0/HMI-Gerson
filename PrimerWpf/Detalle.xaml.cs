﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO.Ports;
using PrimerWpf.Clases_Generales;

namespace PrimerWpf
{
    public partial class Detalle : Window
    {
        #region Variables
        public Controles controles = new Controles();
        private ControlSwitch wpfActivarDesactivar;
        private ComunicacionSerial comunicacionMicro;
        private Modelo modelo = new Modelo();
        private enumEstaciones estacion;
        #region Seccionadores
        public const string sc1 = "SC1";
        public const string sc2 = "SC2";
        public const string sc3 = "SC3";
        public const string sc4 = "SC4";
        public const string sc5 = "SC5";
        public const string sc6 = "SC6";
        public const string sc7 = "SC7";
        public const string sc8 = "SC8";
        public const string sc9 = "SC9";
        public const string sc10 = "SCA";
        public const string sc11 = "SCB";
        public const string sc12 = "SCC";
        public const string ONSC = "ON";
        public const string OFFSC = "OF";
        #endregion
        #region Voltaje-Corriente
        public static string Voltaje = "V";
        public static string Corriente = "I";
        #endregion
        #region Estados
        static SolidColorBrush ON = new SolidColorBrush(Colors.Brown);
        static SolidColorBrush OFF = new SolidColorBrush(Colors.LightGray);
        #endregion
        #endregion
        #region Constructores
        public Detalle()
        {
            InitializeComponent();
        }
        public Detalle(ComunicacionSerial comunicacionSerial, enumEstaciones _estacion = enumEstaciones.Principal)
        {
            InitializeComponent();
            this.DataContext = controles;
            estacion = _estacion;
            controles.Estacion = ObtenerDescripcion(estacion);
            comunicacionMicro = comunicacionSerial;
            comunicacionMicro.SetMetodos(DatosPorSerial, ErrorPorSerial);
            ObtenerEstadosDeDispositivos();
        }
        #endregion
        #region Propiedades
        public ControlSwitch FrmActivarDesactivar {
            get{
                return wpfActivarDesactivar;
            }
            set{
                if(wpfActivarDesactivar != value){
                    wpfActivarDesactivar = value;
                }
            }
        }
        #endregion
        #region Metodos
        private void DatosPorSerial(object sender, SerialDataReceivedEventArgs e)
        {
            string mensaje = ((SerialPort)sender).ReadTo("$");
            if (mensaje.Contains(Voltaje) && mensaje.Length > 4)
            {
                controles.Voltaje = mensaje.Substring(0, 4) + " Volt";
            }
            else if (mensaje.Contains(Corriente) && mensaje.Length > 4)
            {
                controles.Corriente = mensaje.Substring(0, 4) + " Amp";
            }
        }
        private void ErrorPorSerial(object sender, SerialErrorReceivedEventArgs e)
        {
        }
        private void Button_Click(object sender, RoutedEventArgs e){
            LlamarAFormularioOnOff(TipoDeSwitch.Disyuntor);
        }
        private void SC1_Click(object sender, RoutedEventArgs e)
        {
            LlamarAFormularioOnOff(TipoDeSwitch.Seccionador1);
        }
        private void SC2_Click(object sender, RoutedEventArgs e)
        {
            LlamarAFormularioOnOff(TipoDeSwitch.Seccionador2);
        }
        private void LlamarAFormularioOnOff(TipoDeSwitch tipoDeSwitch)
        {
            FrmActivarDesactivar = new ControlSwitch(ref comunicacionMicro, estacion);
            FrmActivarDesactivar.Switch = tipoDeSwitch;
            FrmActivarDesactivar.ShowDialog();
            ObtenerEstadosDeDispositivos();
        }
        private void ObtenerEstadosDeDispositivos()
        {

            if (modelo.EstadoDispositivosDisyuntor((int)estacion))
                controles.Dyt = new SolidColorBrush(Colors.Brown);
            else
                controles.Dyt = new SolidColorBrush(Colors.LightGray);
            if (modelo.EstadoDispositivosSeccionador1((int)estacion))
                controles.Sw1 = new SolidColorBrush(Colors.Brown);
            else
                controles.Sw1 = new SolidColorBrush(Colors.LightGray);
            if (modelo.EstadoDispositivosSeccionador2((int)estacion))
                controles.Sw2 = new SolidColorBrush(Colors.Brown);
            else
                controles.Sw2 = new SolidColorBrush(Colors.LightGray);
        }
        private string ObtenerDescripcion(enumEstaciones estaciones)
        {
            var values = estaciones.GetType().GetField(estaciones.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (values[0] as DescriptionAttribute).Description;
        }
        #endregion
    }
    #region SubClases




    #endregion
}
