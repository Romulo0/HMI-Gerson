﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.IO.Ports;
using PrimerWpf.Clases_Generales;

namespace PrimerWpf
{
    public partial class MainWindow : Window
    {
        #region Estados
        static SolidColorBrush ON = new SolidColorBrush(Colors.Brown);
        static SolidColorBrush OFF = new SolidColorBrush(Colors.LightGray);
        #endregion
        ComunicacionSerial comunicacionSerial;
        IndicadoresDeEstacionPrincipal estados = new IndicadoresDeEstacionPrincipal();
        Detalle formDetalle;
        #region Constructores
        public MainWindow()
        {
            //   InitializeBD();
            if (!InitializeSesion())
            {
                App.Current.Shutdown();
            }
            InitializeComponent();
            InitializerStates();
            comunicacionSerial = new ComunicacionSerial();
            this.DataContext = estados;
        }
        #endregion
        #region Funciones de Inicializacion
        private void InitializerStates() {
            estados.Sw1Principal = OFF;
            estados.Sw1Primera = OFF;
            estados.Sw1Segunda = OFF;
            estados.Sw1Tercera = OFF;
            estados.Sw1Cuarta = OFF;
            estados.Sw1Quinta = OFF;
            estados.Sw2Principal = OFF;
            estados.Sw2Primera = OFF;
            estados.Sw2Segunda = OFF;
            estados.Sw2Tercera = OFF;
            estados.Sw2Cuarta = OFF;
            estados.Sw2Quinta = OFF;
        }
        private void InitializeBD() {
            //using (var db = new ModelEstacionContainer()) {
            //    var queryResult = from user in db.UsuariosSet
            //                      select user;
            //    if (queryResult.Count() > 0) return;
            //    var _user = new Usuarios { Username = "", Password = ""};
            //    var queryResult = from userAccionTable in db.UserAccionsSet
            //                      select userAccionTable;
            //    if (queryResult.Count() > 0) return;
            //    var _userAccion = new UserAccions { Username = "", Password = "" };
            //    var queryResult = from signUp in db.SignUpSet
            //                      select signUp;
            //    if (queryResult.Count() > 0) return;
            //    var _signUp = new SignUp { Username = "", Password = "" };
            //}
        }
        private bool InitializeSesion()
        {
            SignUpForm signUp;
            SignIn signIn;
            do
            {
                signUp = new SignUpForm();
                signIn = new SignIn();
                signUp.ShowDialog();
                if (signUp.ToSignIn)
                {
                    signIn.ShowDialog();
                }
            } while (signUp.ToSignIn && signIn.ToSignUp);
            return signUp.SignUpIsValid;
        }
        #endregion
        #region Eventos Click
        private void btnDTY1_Click(object sender, RoutedEventArgs e)
        {
            try{ llamaAFormDetalle(enumEstaciones.Principal); }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnDTY2_Click(object sender, RoutedEventArgs e)
        {
            try { llamaAFormDetalle(enumEstaciones.Estacion1); }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnDTY3_Click(object sender, RoutedEventArgs e)
        {
            try { llamaAFormDetalle(enumEstaciones.Estacion2); }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnDTY4_Click(object sender, RoutedEventArgs e)
        {
            try { llamaAFormDetalle(enumEstaciones.Estacion3); }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnDTY5_Click(object sender, RoutedEventArgs e)
        {
            try { llamaAFormDetalle(enumEstaciones.Estacion4); }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btnDTY6_Click(object sender, RoutedEventArgs e)
        {
            try { llamaAFormDetalle(enumEstaciones.Estacion5); }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        #endregion
        private void llamaAFormDetalle(enumEstaciones formId) {
            try
            {
                string numeroEstacion = ((int)formId).ToString();
                comunicacionSerial.EnviarMensaje(numeroEstacion);
                formDetalle = new Detalle(comunicacionSerial, formId);
                formDetalle.ShowDialog();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            App.Current.Shutdown();
        }
    }
}
