﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace PrimerWpf.Clases_Generales
{
    public enum enumEstaciones
    {
        [Description("Estacion Principal")]
        Principal =1,
        [Description("Estacion 1")]
        Estacion1,
        [Description("Estacion 2")]
        Estacion2,
        [Description("Estacion 3")]
        Estacion3,
        [Description("Estacion 4")]
        Estacion4,
        [Description("Estacion 5")]
        Estacion5
    }
    public enum enumAcciones
    {
        DisyuntorOff = 1,
        DisyuntorOn,
        Seccionador1Off,
        Seccionador1On,
        Seccionador2Off,
        Seccionador2On
    }
}
