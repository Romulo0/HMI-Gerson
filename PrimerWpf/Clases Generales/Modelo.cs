﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimerWpf.Clases_Generales
{
    class Modelo
    {
        public bool EstadoDispositivosDisyuntor(int estacion)
        {
            try
            {
                using (var db = new ModelEstacionContainer())
                {
                    var EstadoDisyuntor = (from userActions in db.UserAccionsSet
                                           join acciones in db.AccionesSet
                                           on userActions.Accion equals acciones.Id
                                           where userActions.Accion < 3 && userActions.IdEstacion == estacion
                                           select new { acciones.Estado, userActions.Id }).ToList();
                    EstadoDisyuntor.Sort((obj1, obj2) => obj1.Id.CompareTo(obj2.Id));
                    if (EstadoDisyuntor.Any())
                        return EstadoDisyuntor.Last().Estado;
                    else return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool EstadoDispositivosSeccionador1(int estacion)
        {
            try
            {
                using (var db = new ModelEstacionContainer())
                {
                    var EstadoSeccionador1 = (from userActions in db.UserAccionsSet
                                              join acciones in db.AccionesSet
                                              on userActions.Accion equals acciones.Id
                                              where (userActions.Accion == 3 || userActions.Accion == 4) && userActions.IdEstacion == (int)estacion
                                              select new { acciones.Estado, userActions.Id }).ToList();
                    EstadoSeccionador1.Sort((obj1, obj2) => obj1.Id.CompareTo(obj2.Id));
                    if (EstadoSeccionador1.Any())
                        return EstadoSeccionador1.Last().Estado;
                    else return false;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool EstadoDispositivosSeccionador2(int estacion)
        {
            try
            {
                using (var db = new ModelEstacionContainer())
                {
                    var EstadoSeccionador2 = (from userActions in db.UserAccionsSet
                                              join acciones in db.AccionesSet
                                              on userActions.Accion equals acciones.Id
                                              where (userActions.Accion == 5 || userActions.Accion == 6) && userActions.IdEstacion == (int)estacion
                                              select new { acciones.Estado, userActions.Id }).ToList();
                    EstadoSeccionador2.Sort((obj1, obj2) => obj1.Id.CompareTo(obj2.Id));
                    if (EstadoSeccionador2.Any())
                        return EstadoSeccionador2.Last().Estado;
                    else return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<bool> BuscarUsuario(string usuario, string password)
        {
            try
            {
                using (var db = new ModelEstacionContainer())
                {
                    var queryResult = (from user in db.UsuariosSet
                                       where user.Username == usuario && user.Password == password
                                       select user.Id).ToList();
                    if (queryResult.Count() != 1)
                    {
                        return false;
                    }
                    else
                    {
                        db.SignUpSet.Add(new SignUp
                        {
                            IdUsuario = queryResult[0],
                            FechaDeInicio = DateTime.Now,
                            FechaDeCierre = DateTime.Parse("9/9/9999")
                        });
                        await db.SaveChangesAsync();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task InsertarUsuario(string usuario, string password)
        {
            try
            {
                using (var db = new ModelEstacionContainer())
                {
                    var queryResult = from user in db.UsuariosSet
                                      where user.Username == usuario
                                      select user.Id;
                    if (queryResult.Count() > 0)
                    {
                        throw new Exception("El usuario ya existe");//MessageBox.Show("El usuario ya existe");
                    }
                    db.UsuariosSet.Add(new Usuarios
                    {
                        Username = usuario,
                        Password = password
                    });
                    await db.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task InsertarAccionDeUsuario(int TipoDeAccion, int estacion)
        {
            using (var db = new ModelEstacionContainer())
            {
                var result = (from session in db.SignUpSet
                              select session.Id).Max();
                db.UserAccionsSet.Add(new UserAccions
                {
                    Accion = TipoDeAccion,
                    FechaDeEjecucion = DateTime.Now,
                    IdSignUp = result,
                    IdEstacion = estacion
                });
                await db.SaveChangesAsync();
            }
        }
    }
}
