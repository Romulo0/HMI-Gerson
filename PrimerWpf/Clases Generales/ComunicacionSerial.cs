﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PrimerWpf.Clases_Generales
{
    public class ComunicacionSerial
    {
        #region Variables
        private SerialPort portMicro = new SerialPort();
        private SerialDataReceivedEventHandler datosPorSerial;
        private SerialErrorReceivedEventHandler errorPorSerial;
        #endregion
        #region Constructores
        public ComunicacionSerial()
        {
            InitializeSerialPort();
        }
        public ComunicacionSerial(SerialDataReceivedEventHandler datosPorSerial, SerialErrorReceivedEventHandler errorPorSerial)
        {
            this.datosPorSerial = datosPorSerial;
            this.errorPorSerial = errorPorSerial;
            InitializeSerialPort(DatosPorSerial, ErrorPorSerial);
        }
        #endregion
        #region Propiedades
        SerialDataReceivedEventHandler DatosPorSerial
        {
            get { return datosPorSerial; }
            set
            {
                if(datosPorSerial != value)
                {
                    datosPorSerial = value;
                }
            }
        }
        SerialErrorReceivedEventHandler ErrorPorSerial
        {
            get { return errorPorSerial; }
            set
            {
                if (errorPorSerial != value)
                {
                    errorPorSerial = value;
                }
            }
        }
        #endregion
        #region Metodos
        public void SetMetodos(SerialDataReceivedEventHandler datosPorSerial, SerialErrorReceivedEventHandler errorPorSerial)
        {
            this.datosPorSerial = datosPorSerial;
            this.errorPorSerial = errorPorSerial;
            portMicro.DataReceived += DatosPorSerial;
            portMicro.ErrorReceived += ErrorPorSerial;
        }
        private void InitializeSerialPort(
            SerialDataReceivedEventHandler datosPorSerial = null,
            SerialErrorReceivedEventHandler errorPorSerial = null
        )
        {
            try
            {
                portMicro.BaudRate = 9600;
                portMicro.DataBits = 8;
                portMicro.Parity = Parity.None;
                portMicro.StopBits = StopBits.One;
                portMicro.PortName = "COM2";
                if(DatosPorSerial != null && ErrorPorSerial != null) { 
                    portMicro.DataReceived += DatosPorSerial;
                    portMicro.ErrorReceived += ErrorPorSerial;
                }
                portMicro.Open();
                portMicro.Write("O");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public void EnviarMensaje(string Mensaje)
        {
            portMicro?.Write(Mensaje);
        }
        #endregion
    }
}
